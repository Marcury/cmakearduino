###################################
## Project configuration
###################################

cmake_minimum_required(VERSION 3.10)
project(HAL_CPP_ATMEGA328P CXX)

#For C project
set(CMAKE_C_STANDARD 90)
set(CMAKE_C_STANDARD_REQUIRED True)

#For CPP project
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

#Include toolchain
#include(${CMAKE_SOURCE_DIR}/atmega328p_toolchain.cmake) #Saddly, I didn't managed to make
				#the --toolchain option do what I wanted. Which was keeping
				#the target and option specific to cross compiling in a
				#different file. But just including the file works well
				# since it is just liake pasting it into the main CMakelist

###################################
## Setting common compile options
###################################

###Common Compile options

#Ok there is a lot of compile option. But it push you to stick to code standard.
#It should make you avoid writting non portabble codes. But yeah, it is pretty boring
#to have the compiler barking at you for nothing.

add_compile_options(
	-Wall			#basic warnings
	-Wextra			#more warning so your code is better :D
#	-Wstrict-prototypes	#for instance void argument for a method must be
				#indicated: int myMethod(void). Not available with g++

	-Wunreachable-code	#warn you when a part of the code can't be reached
	-Wundef			#warning if there are undefined wariables
	-Wshadow		#If a variable shadow another one
	-Os			#Optimise. You might want to disable optimisation if you
				#don't the compiler to mess up with your code

	-Werror			#Warnin are treated as error. It is very boring but makes
				#force you to code better

	-Wfatal-errors		#Quit after the first error. Honestly maybe I should
				#disable this one

	-pedantic		#Compiler is strict regarding languages rule syntax
	-Wno-main		#No warning from main. Maybe a bad thing.
#	-g			#For debugging
#	-gwarf			#Debugging with DWARF format
	-Wl,--relax,--gc-sections	#Remove unused part that the linker don't use.
				#Great for code size optimisation
        -Wpointer-arith         #Warn an operation is performed on
        -Wfloat-equal           #Warn if equal comparaison on float
#       -Wstrict-overflow=5     #Warnn if compiler can optimise, assuming that there is no
                                #overflow

        -Wcast-qual             #const must be conserved when casting
        -Wconversion            #warn when implicit conversion that may lead to data loss
        -Waggregate-return      #Warn when struct or aggregate type are returned. It is
                                #better to return pointer or references
        -Wswitch-default        #switch instruction must have a default statement
        -Wswitch-enum)          #Warn when a switch doesn't have a statement for each member
                                #of an enum.

#####################################
## Targets
#####################################

add_executable(${PROJECT_NAME} main.cpp)
#Output file renamming

if(CMAKE_CROSSCOMPILING)
	set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME ${PROJECT_NAME}.hex)

	#Converting binary file to hex file
	add_custom_target(hex ALL avr-objcopy -O ihex ${PROJECT_NAME}.hex DEPENDS
	${PROJECT_NAME})

	#Uploading the hex file to the board
	add_custom_target(upload avrdude -v -patmega328p -c${BOARD}
                 "-P${USB}" -b${BAUD} -D "-Uflash:w:${PROJECT_NAME}.hex:i" DEPENDS hex)
endif()
#####################################
## Project Structure
#####################################

target_include_directories(${PROJECT_NAME} PUBLIC includes)
target_sources(${PROJECT_NAME} PUBLIC ${CMAKE_SOURCE_DIR}/src/blink.cpp)
