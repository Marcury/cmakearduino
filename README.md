# cmakearduino

This is literally my first makefile on a public git, so it will probably not be very...
professional. Feel free to give me advice on how to improve it :) Even if it is just about 
how to write README or not making to much commit or whatever you want. This process is 
specially for distribution based on Ubuntu, but note that the exact version I'm using 
is PopOS (which is based on ubuntu). But I guess it should works on other linuw distrib
has long has you have AVR GNU toolchain, avrdude and cmake (or juste use Makefile)

It is inspired by several thing I found on internet. The most effective way to achieve a 
compilation and upload on the Arduino board without the IDE (or using an other one) is to
activate verbose on ArduinoIDE and to look at what it is doing.

## Needed tool

see the version I used at the end of the README

avrdude (to upload file)
avr-gcc and avr-libc(the tool-chain to compile the code)
cmake

# Compiling C or C++ code with command line

## Compiling C
This command compile a unique .c file into a .out file
	avr-gcc -mmcu=atmega328p main.c
This one transform the .out file into a .hex file, which is the type of file we want to upload
on the board. It basically contain our code.
	avr-objcopy -O ihex -j.text -j.data a.out main.hex


# Uploading code on a arduino UNO

Avrdude is a tool used by arduino themself to send hex file to the board. The CMakeLists.txt
should call it to send the generated files to the board.

## Command line
	avrdude -v -patmega328p -carduino "-P/dev/ttyACM0" -b115200 -D "-Uflash:w:main.hex:i"
Note that the "-P/dev/ttyACM0" option might be different for you. You have to figure out 
which device under /dev/ you need to target (just unplug, use ls, plug , use ls again and
find the device that appeared). 

# The CMakeLists

The CMakelist contain a bunch of comment that explain why the line is added to the file.
It basically define a CMakeProject, some constant used in the avrdude and avr-gcc command 
and then a lot of compiling options. It also define custom target used to make the .hex file
and upload it to the arduino board. Note that it is possible to remove literally all flag
I have added (except the -mmcu one). Those flag are there because they help the developper to
write a more "reliable code" that is supported on most of the MCU.

Since I'm far from being an expert in CMakeLists.txt, I'm open to suggestion, even if it just
concern the Project Structure part or the Project Setup part.

It should even allow you to use it in a IDE that handle CMake based project (CLion or VSCode
for instance) and upload your code to the board without having to install plugins that make
whatever you don't know it is to the IDE.

## How to use it
	
	mkdir build
	cd build
	cmake --toolchain ../atmega328p_toolchain.cmake ..
	make upload

And you uploaded the code to the target.

Of course, in order to use the Cmakelist, you'll have to change the name of the project,
the target, the sources and includes of the target to use yours.

## Future update on the CMakeLists.txt?

For now, it works for C. And it doesn't alow .a and .dll management. That something i'll
work on later.
Same for C++ since I plan to use it to write a complete ArduinoHAL (to acess register and
not a delay that use whatever timer it wants)

# Used tools

avr-gcc:	avr-gcc (GCC) 5.4.0
avrdude:	version 6.3-20171130
cmake:		version 3.22.1

target mcu:	Atmega328p
arduino board: 	Arduino UNO R3
OS:		Pop!_OS 22.04 LTS (Ubuntu 22.04 based)
