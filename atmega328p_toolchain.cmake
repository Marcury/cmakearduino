###################################
## Setting AVR Toolchain
###################################

set(CMAKE_SYSTEM_NAME Generic)			#OS. Generic for Bare Metal
set(CROSS_COMPILE avr-)				#ABI prefix
set(MCU atmega328p)				#Used µC. atmega328p for some arduino

###################################
## Setting Board option
###################################

set(BOARD arduino)
set(BAUD 115200)
set(USB "/dev/ttyACM0")

###################################
## Setting Compilers
###################################


#Complete path is needed if those are not in your $PATH env variable
set(CMAKE_C_COMPILER ${CROSS_COMPILE}gcc)			#Setting C Compiler.
set(CMAKE_CXX_COMPILER ${CROSS_COMPILE}g++)			#Setting C++ Compiler.
set(CMAKE_LINKER ${CROSS-COMPILE}ld)
set(CMAKE_AR ${CROSS-COMPILE}ar)
set(CMAKE_OBJCOPY /bin/${CROSS_COMPILE}objcopy CACHE FILEPATH "Toolchain objcopy command"
FORCE)

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

#AVR Compile options

add_compile_options(
	-mmcu=${MCU}
        -mno-interrupts         #if you plan to not use interrup (makes you code smaller)
#       -maccumulate-args       #if you plan to make a chained function call. 
                                #Compiler will try to keep return argument of function in
                                #the stack. Lead to smaller executable. But I guess you
                                #might get a stack overflow much faster if you chain 
                                #function call too much
                                #Float and double size is 32 bits on Atmega328p. You should
                                #use the correct type double32_t and long-double32_t
#        -mint8                  #But int will be 8bit, char too, long 2b and long long 4b
#        -Wmisspelled-isr        #Warn if isr is misspeled
#        -Waddr-space-convert    #Warn if cast between Heap and Stack
        )

###################################
## Setting Find command Option
###################################

# Set directories where the find command will search for file, library or compiler.
set(CMAKE_FIND_ROOT_PATH  /bin/avr-gcc		#Path to toolcahin (compiler and library)
	/usr/lib/gcc/avr/5.4.0/)

#Search programs in the host env
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

#Set CMAKE headers and libraries in the target env.
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
