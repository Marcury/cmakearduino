/*
*@brief: Here again, this code is not mine. And all the comment too. I'm only using it as a test.
*/

#ifndef BLINK_H
# define BLINK_H

/*This is a pointer to the location in memory at hex number 0x25
*it is a 1-byte register defined by ATmega328p that controls the physical pins 14 thru 19 (mapping to bits 0-5)
*setting a bit to 0 means it's set to low, setting a bit to 1 means it's high
*setting it to volatile to alert the compiler this variable can change outside 
*my code, so it doesn't optimize out this value
*/
#define PORTB *((volatile unsigned char *)0x25)

/*this is a pointer to the location in memory at hex 0x24
*this is the Data-direction register for PortB. It controls physical pins 14 thru 19 (mapping to bits 0-5)
*setting a bit to 0 means the pin is set to input mode, setting to 1 means it's output
*setting it to volatile to alert the compiler this variable can change outside 
*my code, so it doesn't optimize out this value
*/
#define DDRB *((volatile unsigned char *)0x24)

void setup(void);

#endif /* BLINK_H */
