#ifndef BLINK_H
#define BLINK_H

#define PORTB *((volatile unsigned char *)0x25)
#define DDRB *((volatile unsigned char *)0x24)

namespace A{
	class Led{
	   public:
		Led(){};		//Constructor
		void setup();
		~Led() = default;				//Destructor
	};
}

#endif /* BLINK_H */
