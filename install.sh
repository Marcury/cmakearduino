# !/bin/bash
# Install required tool to flash code into the Arduino UNO R3

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [--no-color]

Install required executables to compile and upload project on Arduino UNO R3 using cmake.

Available options:

-h, --help      Print this help and exit
--no-color	Script won't use color
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
  echo -e "${YELLOW}End of the script.${NOFORMAT}"
  exit
}

setup_colors() {
  if [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

errmsg() {
  echo >&2 -e "${1-}"
}

die() {
  local errmsg=$1
  local code=${2-1} # default exit status 1
  errmsg "$errmsg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  flag=0
  param=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    --no-color) NO_COLOR=1 ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  return 0
}

check_root() {
  if [[ "$EUID" -ne 0 ]]; then
    die "${RED}Must be launch with sudo.${NOFORMAT}"
  fi
}


parse_params "$@"
setup_colors
check_root


# script logic here

echo -e "${YELLOW}install.sh start.${NOFORMAT}"

apt update
apt install -y cmake gcc-avr avr-libc avrdude
