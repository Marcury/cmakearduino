/*This test code come from this site: https://daniellethurow.com/blog/2021/6/8/programming-an-atmega328p-without-the-arduino-ide
* It also contain a lot of instruction telling how to upload code on the board.*/

#include "led.h"

int main(void)
{
    using namespace A;
    long i;
    Led a = Led(2);
    a.setup();
    /* loop */
    while (1)
    {

        
        /*by setting the 6th bit on this register to 1
        *It is setting the bit that controls physical pin 13, and setting it to high
        *using bitwise OR to turn on the 6th bit without affecting the other bits in PORTB*/

	PORTB |= 0x20;

        /*go to sleep for an arbitrary amount of time*/
        for (i = 0; i < 10000; i++)
        {
            /*to make sure the compiler doesn't optimize this out, continue
            *setting the 6th bit of portb to 1*/

            PORTB |= 0x20;
        }
        /*setting PortB 6th bit to 0 sets pins PortB controls to low */

        PORTB &= 0xDF;

        /*go to sleep for an arbitrary amount of time */
        for (i = 0; i < 1000; i++)
        {
            /*to make sure the compiler doesn't optimize this out, continue
            *setting PORTB 6th bit to 0 */

            PORTB &= 0xDF;

        }
    }
}

