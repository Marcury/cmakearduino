#include "blink.h"

void setup(void){
    /*setup
    *by setting the 6th bit on this register to 1
    *It is setting the bit that controls physical pin 13, and saying it is an 
    *using bitwise OR to turn on the 6th bit without affecting the other bits */
    DDRB |= 0x20;
}
